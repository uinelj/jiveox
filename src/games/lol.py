import os
import datetime
import logging
import json

import riotwatcher
import requests

from .utils import unix_time_millis

apikey = os.environ["LOL_APIKEY"]
log = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


class LeagueNews(riotwatcher.RiotWatcher):
    """
        RiotWatcher wrapper.

        Adds some "sugar" to ease development of features.
    """

    def __init__(self, **kwargs):
        """
            Create a LeagueNews object.

            :param region: Region to use. Example : 'euw1' 
            :param api_key: Riot API Key
        """
        try:
            self.region = kwargs.pop("region")
        except AttributeError as e:
            log.warn("region attribute not set !")
        super(LeagueNews, self).__init__(**kwargs)

    def _champion_from_id(self, id):
        try:
            return self._champions[id]
        except (AttributeError, KeyError) as e:
            log.info("Champion info not found ! Getting champion info")
            url = (
                "http://ddragon.leagueoflegends.com/cdn/8.21.1/data/en_US/champion.json"
            )
            champion_info = requests.get(url).json()
            self._champions = {}
            for champion, info in champion_info["data"].items():
                # log.debug(info)
                self._champions[info["key"]] = info["id"]
            return self._champions[id]

    def _last_games(self, user, start_date=None):
        """
            Gets games of user from start_date to now.

            :param user: User
            :param start_date: Date to start, in datetime.

            :returns: Matchlist for the specified account
        """

        if start_date is None:
            start_date = datetime.datetime.now() - datetime.timedelta(days=1)
            print("Start date is " + str(start_date))

        # Converting dates
        start_date = unix_time_millis(start_date)
        # return self.matchlist_by_account(self.region, begin_time=start_date, end_time=end_date)
        try:
            matches = self.match.matchlist_by_account(
                self.region, user["accountId"], begin_time=start_date
            )

            game_ids = [game['gameId'] for game in matches['matches']]

            return game_ids
        except requests.exceptions.HTTPError as e:
            log.debug(e)
            log.warn("Could not load games for summoner {}".format(user['name']))
            return None

    def _get_game_stats(self, game_id):
        """
            Gets KDA stats for one game.
            Returns a list of participants, along with their score and champion.
            :param game_id: id of the game
            :returns: list of participants
        """
        game_stats = self.match.by_id(self.region, game_id)
        log.debug(game_stats['gameMode'])

        participants = []
        id_to_nick = {}

        for participant in game_stats['participantIdentities']:
            id = participant['participantId']
            nick = participant['player']['summonerName']
            id_to_nick[id] = nick

        for participant in game_stats['participants']:
            participants.append({
                'gameId': game_stats['gameId'],
                'nickname': id_to_nick[participant['participantId']],
                'championId': participant['championId'],
                'kills': participant['stats']['kills'],
                'deaths': participant['stats']['deaths'],
                'assists': participant['stats']['assists'],
            })

        return participants

    def get_mastered_champions(self, nickname, limit=3, fields=None):
        if fields is None:
            fields = ["championId", "championLevel", "championPoints", "chestGranted"]
        summoner = self.summoner.by_name(self.region, nickname)
        mastered_champions = self.champion_mastery.by_summoner(self.region, summoner["id"])[:limit]

        # For each champion in the mastered_champions list, we prune keys that aren't in the fields list.
        mastered_champions = [
            {k: v for k, v in champion.items() if k in fields}
            for champion in mastered_champions
        ]
        log.debug(mastered_champions)
        for champion in mastered_champions:
            # champion['name'] = self.champion_from_id(champion['championId'])['name']
            champion["name"] = self._champion_from_id(str(champion["championId"]))
            del champion["championId"]

        return mastered_champions

    def get_games_digest(self, users, start_date=None):
        """
            Gets a digest of the last games of a group of users.
        """
        
        digest = {}

        # Get game data
        game_ids = set()
        for user in users:
            try:
                user = self.summoner.by_name(self.region, user)
                last_games = self._last_games(user)
                if last_games is not None:
                    game_ids.update(self._last_games(user))
            except requests.HTTPError as e:
                if e.response.status_code == 403:
                    log.error("Unable to access Riot API. Check API Key.")
                else:
                    log.error(e)
                    log.error("User {} not found".format(user))

        log.info("Got {} games".format(len(game_ids)))
        if len(game_ids) == 0:
            return None
        digest['nb_games'] = len(game_ids) 
        # Remove unwanted participants
        # Calculate KDA
        for game_id in game_ids:
            stats = self._get_game_stats(game_id)
            stats = [player for player in stats if player['nickname'] in users]
            for stat in stats:
                if stat['deaths'] == 0:
                    stat['deaths'] = 1
                stat['kda'] = ((stat['kills']) + stat['assists']) / stat['deaths']
                if 'best_kda' not in digest.keys() or stat['kda'] > digest['best_kda']:
                    digest['best_kda'] = stat['kda']
                    digest['best'] = stat
                if 'worst_kda' not in digest.keys() or stat['kda'] < digest['worst_kda']:
                    digest['worst_kda'] = stat['kda']
                    digest['worst'] = stat

        digest['best']['championId'] = self._champion_from_id(str(digest['best']['championId']))
        digest['worst']['championId'] = self._champion_from_id(str(digest['worst']['championId']))
        log.info("Best performance :{}".format(digest['best']))
        log.info("Worst performance :{}".format(digest['worst']))

        return digest
                              

if __name__ == "__main__":

    ln = LeagueNews(api_key=apikey, region="euw1")

    # ln = riotwatcher.RiotWatcher(apikey)

    me = ln.summoner.by_name("euw1", "Dr Le Foute")
    # print(me)
    # print(ln._last_games(me))
    # print(ln._get_game_stats('3721273699'))
    copains = [
        "Dr LE FOUTE",
        "Petite Nature",
        "Dr BlueBalls",
        "YoyoshiDuFutur"
    ]
    # print(ln.get_games_digest(copains))
    # print(ln.get_mastered_champions("Dr Le Foute", limit=5))
