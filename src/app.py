import os
import logging
import datetime
import asyncio

import discord

from db import UserManager
from games.lol import LeagueNews

log = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

token = os.environ["DISCORD_TOKEN"]
db_params = {
    "host": os.environ["REDIS_URL"],
    "port": os.environ["REDIS_PORT"],
    "password": os.environ["REDIS_PASS"],
    "db": 0,
}

client = discord.Client()
db = UserManager(**db_params)

ln = LeagueNews(api_key=os.environ["LOL_APIKEY"], region="euw1")

async def gazette():
    await client.wait_until_ready()
    channel = discord.Object(id=193468278457237504)
    while not client.is_closed:
        msg = await client.send_message(channel, "Crunching data...")
        users = db.smembers("lolnews")
        users = [user.decode("utf-8") for user in users]
        log.info("Getting news for users : {}".format(users))
        digest = ln.get_games_digest(users)
        today = datetime.date.today().strftime("%d %B")
        game_nb = digest['nb_games']
        winrate = 0

        best = "{} ({}) : {}/{}/{} ({}) [LoG](https://www.leagueofgraphs.com/match/euw/{})".format(
            digest['best']['nickname'],
            digest['best']['championId'],
            digest['best']['kills'],
            digest['best']['deaths'],
            digest['best']['assists'],
            digest['best']['kda'],
            digest['best']['gameId']
        )
        worst = "{} ({}) : {}/{}/{} ({}) [LoG](https://www.leagueofgraphs.com/match/euw/{})".format(
            digest['worst']['nickname'],
            digest['worst']['championId'],
            digest['worst']['kills'],
            digest['worst']['deaths'],
            digest['worst']['assists'],
            digest['worst']['kda'],
            digest['worst']['gameId']
        )

        description="""
            Près de {} games jouées, totalisant un winrate de {}%.
            A demain pour de nouvelles aventures !

            """.format(
            game_nb,
            winrate
        )

        embed = discord.Embed(
            title="Edition du {}".format(today),
            colour=discord.Colour(0x8080),
            url="https://discordapp.com",
            description=description,
            # timestamp=datetime.datetime.utcfromtimestamp(1533322776)
        )

        # embed.set_image(url="https://cdn.discordapp.com/embed/avatars/0.png")
        embed.set_thumbnail(url="https://gitlab.com/uinelj/disord/raw/master/assets/bot.png")
        embed.set_author(
            name="Gazette du Feeder",
            # url="https://discordapp.com",
            # icon_url="https://cdn.discordapp.com/embed/avatars/0.png",
        )
        embed.set_footer(
            text="uj e jonti",
            # icon_url="https://cdn.discordapp.com/embed/avatars/0.png",
        )

        embed.add_field(name="Meilleure performance", value=best)
        embed.add_field(name="Meilleure merde", value=worst)

        await client.edit_message(
            msg, new_content="**La Gazette du feeder**", embed=embed
        )
        await asyncio.sleep(86400)

@client.event
async def on_message(message):

    if message.content.startswith("!debug"):
        channels = list(client.servers)[0].channels
        channels = [(channel.name, channel.id) for channel in channels]
        await client.send_message(message.channel, channels)
    
    # Don't answer to non private messages
    if not message.channel.is_private:
        return

    # We don't want to respond to ourself
    if message.author == client.user:
        return

    # !help displays command help
    if message.content.startswith("!help"):
        msg = "Avaliable commands:\n"
        msg += "- `!players` : List players\n"
        msg += "- `!list` : List games\n"
        msg += "- `!sub game` : Subscribe to game\n"
        msg += "- `!sub` : List subscribed games\n"
        msg += "- `!unsub game` : Unsubscribe to game\n"
        msg += "- `!lolmastery nickname` : Get top 3 champions for a given league player\n"
        msg += "- `!loladd nickname` : Link your account to the daily Gazette du Feeder\n"

        await client.send_message(message.channel, msg)

    if message.content.startswith("!list"):

        game_list = {}
        ans = "Games list :\n"
        cursor, users = db.scan(0, "u-*")
        while cursor != 0:
            cursor, new_users = db.scan()
            users.append(new_users)
        for user in users:
            for game in db.smembers(user):
                try:
                    game_list[game.decode('utf-8')] += 1
                except KeyError as e:
                    game_list[game.decode('utf-8')] = 1
        
        for game, number in game_list.items():
            ans += "- **{}** : {} subs\n".format(game, number)

        await client.send_message(message.channel, ans)

        
    # !players lists currently playing users
    # TODO: Ignore bots
    if message.content.startswith("!players"):
        members = list(client.servers)[0].members
        members_playing = [member for member in members if member.game is not None]

        msg = ""
        for member in members_playing:
            msg += "* {} - {}\n".format(member, member.game)

        await client.send_message(message.channel, msg)

    # Subscribe to a game
    # If no args are set, list subscribed games
    if message.content.startswith("!sub"):

        user = message.author
        try:
            subscription = message.content.split(" ")[1]
            db.add_interest(user, subscription)
            answer = "Subscribed to {}".format(subscription)
        except IndexError:
            interests = db.get_interests(user)
            answer = "Currently subscribed to :\n\n"
            for interest in interests:
                answer += "- {}\n".format(interest.decode("utf-8"))

        await client.send_message(message.channel, answer)

    # Unsubscribe to a game
    if message.content.startswith("!unsub"):

        user = message.author
        subscription = message.content.split(" ")[1]
        db.rm_interest(user, subscription)

        ans = "Removed {} from your subscriptions".format(message.content.split(" ")[1])
        log.info(
            "{} unsubscribed to {}".format(
                message.author, message.content.split(" ")[1]
            )
        )
        await client.send_message(message.channel, ans)

    if message.content.startswith("!news"):
        
        msg = await client.send_message(message.channel, "Crunching data...")
        users = db.smembers("lolnews")
        users = [user.decode("utf-8") for user in users]
        log.info("Getting news for users : {}".format(users))
        digest = ln.get_games_digest(users)
        today = datetime.date.today().strftime("%d %B")
        if digest is None:
            await client.edit_message(
                msg, new_content="No games were played. :/"
            )
            return
        game_nb = digest['nb_games']
        winrate = 0

        best = "{} ({}) : {}/{}/{} ({}) [LoG](https://www.leagueofgraphs.com/match/euw/{})".format(
            digest['best']['nickname'],
            digest['best']['championId'],
            digest['best']['kills'],
            digest['best']['deaths'],
            digest['best']['assists'],
            digest['best']['kda'],
            digest['best']['gameId']
        )
        worst = "{} ({}) : {}/{}/{} ({}) [LoG](https://www.leagueofgraphs.com/match/euw/{})".format(
            digest['worst']['nickname'],
            digest['worst']['championId'],
            digest['worst']['kills'],
            digest['worst']['deaths'],
            digest['worst']['assists'],
            digest['worst']['kda'],
            digest['worst']['gameId']
        )

        description="""
            Près de {} games jouées, totalisant un winrate de {}%.
            A demain pour de nouvelles aventures !

            """.format(
            game_nb,
            winrate
        )

        embed = discord.Embed(
            title="Edition du {}".format(today),
            colour=discord.Colour(0x8080),
            url="https://discordapp.com",
            description=description,
            # timestamp=datetime.datetime.utcfromtimestamp(1533322776)
        )

        # embed.set_image(url="https://cdn.discordapp.com/embed/avatars/0.png")
        embed.set_thumbnail(url="https://gitlab.com/uinelj/disord/raw/master/assets/bot.png")
        embed.set_author(
            name="Gazette du Feeder",
            # url="https://discordapp.com",
            # icon_url="https://cdn.discordapp.com/embed/avatars/0.png",
        )
        embed.set_footer(
            text="uj e jonti",
            # icon_url="https://cdn.discordapp.com/embed/avatars/0.png",
        )

        embed.add_field(name="Meilleure performance", value=best)
        embed.add_field(name="Meilleure merde", value=worst)

        await client.edit_message(
            msg, new_content="**La Gazette du feeder**", embed=embed
        )

    if message.content.startswith("!loladd"):

        summoner_name = " ".join(message.content.split(" ")[1:])
        db.sadd("lolnews", summoner_name)

        await client.send_message(message.channel, "{} will be taken into account when doing gazettes".format(summoner_name))
    if message.content.startswith("!lolmastery"):

        summoner_name = " ".join(message.content.split(" ")[1:])
        masteries = ln.get_mastered_champions(summoner_name)

        ans = "Top 3 most played champions for {}\n".format(summoner_name)
        for champion in masteries:
            ans += "- **{}** : {} pts\n".format(
                champion["name"], champion["championPoints"]
            )

        await client.send_message(message.channel, ans)

    # Notify all members interested in a specified game
    if message.content.startswith("!play"):
        game = message.content.split(" ")[1]
        message = "{} wants to play to {} soon !\n _Type `!unsub {}` to stop being notified._".format(
            message.author, game, game
        )

        for user in db.get_users_from_interest(game):
            name, discriminator = user.decode("utf-8")[2:].split("#")
            discord_user = discord.utils.get(
                client.get_all_members(), name=name, discriminator=discriminator
            )
            log.info("Notifying {}...".format(user))
            await client.send_message(discord_user, message)


@client.event
async def on_member_update(before, after):
    if after.game != before.game:
        # TODO: ignore other bots
        log.info("Member {} game status changed to {}".format(before, after.game))


@client.event
async def on_ready():
    print("Logged in as")
    print(client.user.name)
    print(client.user.id)
    print("------")

    log.info("Setting up game")
    await client.change_presence(game=discord.Game(name="mp me !help pls me alone"))

# client.loop.create_task(gazette())
client.run(token)
