import logging
from redis import StrictRedis as Database

log = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


class UserManager(Database):
    """
        Manages users and subscriptions.
    """

    def __init__(self, **kwargs):
        """
            Initialise the UserManager.
            Same args as redis.StrictRedis

        """
        super(UserManager, self).__init__(**kwargs)

    def get_interests(self, user):
        """
            Returns a list of the user's interests

            :param user: User name + discriminator. Example: foo#1883
        """
        user_key = "u-{}".format(user)
        return self.smembers(user_key)

    def add_interest(self, user, interest):
        """
            Add an interest to a user.

            :param user: User name + discriminator. Example: foo#1883
            :param interest: Interest. Example: lol
        """
        user_key = "u-{}".format(user)
        self.sadd(user_key, interest)

    def rm_interest(self, user, interest):
        """
            Remove interest from a user's interest list
        """
        user_key = "u-{}".format(user)
        self.srem(user_key, interest)

    def get_users_from_interest(self, interest):
        """
            Get users that share an interest
        """
        users = self.keys("u-*")
        interested_users = []
        for user in users:
            interests = [interest.decode("utf-8") for interest in self.smembers(user)]
            if interest in interests:
                interested_users.append(user)
        return interested_users

    def get_subscriptions(self, user):
        """
            Not stable.
            Gets a subscribe thingy from a user
        """
        user_key = "u-{}".format(user)
        subscription_channels = self.smembers(user_key)
        sub = self.pubsub()
        for channel in subscription_channels:
            sub.subscribe(channel)
            log.debug("User {} subscribed to channel {}".format(user_key, channel))
        sub.subscribe("debug")
        return sub

    def subscribe(game):
        pass

    def unsubscribe(game):
        pass


def test_handler(message):
    print("Got message {}".format(message))


if __name__ == "__main__":
    db_params = {"host": "localhost", "port": 6379, "db": 0}
    db = UserManager(**db_params)
    db.add_interest("uj#3701", "csgo")
    db.add_interest("uj#3701", "lol")
    db.add_interest("uj#3701", "dota2")
    sub = db.get_subscriptions("uj#3701")
    print(db.get_interests("uj#3701"))
    while True:
        msg = sub.get_message()
        if msg is not None:
            print(msg)
